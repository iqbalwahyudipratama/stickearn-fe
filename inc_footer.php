    	<!--Footer -->
        <footer>
        	<div class="wrap-md">
        		<div class="foot-top">
        			<div class="left">
        				<div class="footlinks">
        					<div class="ls-foot">
        						<h6>Company</h6>
        						<ul>
        							<li><a href="">About</a></li>
        							<li><a href="">Become Driver</a></li>
        							<li><a href="">Gallery</a></li>
        							<li><a href="">Careers</a></li>
        							<li><a href="">Partners</a></li>
        							<li><a href="">Blog</a></li>
        						</ul>
        					</div>
        					<div class="ls-foot">
        						<h6>Support</h6>
        						<ul>
        							<li><a href="">Contact Us</a></li>
        							<li><a href="">FAQ</a></li>
        							<li><a href="">Privacy & Policy</a></li>
        							<li><a href="">Term of use</a></li>
        							<li><a href="">Term of conditions</a></li>
        						</ul>
        					</div>
        					<div class="ls-foot">
        						<h6>Get In Touch</h6>
        						<div class="box-sosmed">
        							<a href="" class="fb"></a>
        							<a href="" class="ig"></a>
        							<a href="" class="in"></a>
        						</div>
        					</div>
        				</div>
        			</div>
        			<div class="right">
        				<div class="logo-foot">
        					<a href="index.php">
        						<img src="images/material/logo.png" alt="logo footer">
        					</a>
        				</div>
        				<p>Stickearn connects consumer drivers and brands looking for exposure effectively with our modern approach. Our drivers have their cars wrapped in ads and get paid.</p>
        				<div class="phn">
        					<a href="tel:081230888680" class="phone">+6281230888680</a>
        					<a href="mailto:info@stickearn.com" class="mail">info@stickearn.com</a>
        				</div>
        			</div>
        		</div>
        		<div class="foot-bottom">
        			© 2017. PT Paragon Teknologi Indonesia. <br>
        			Jl Letjen Suprapto 400 Cempaka Putih, Jakarta Pusat  10510, Indonesia
        		</div>
        	</div>
        </footer>
        <!--end of Footer -->
    </body>
</html>
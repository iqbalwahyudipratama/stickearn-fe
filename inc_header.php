<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Stickearn</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

        <link rel="icon" href="favicon.ico">

        <!--Style-->
        <!--build:css css/styles.min.css--> 
        <link rel="stylesheet" href="js/slick/slick.css">
        <link rel="stylesheet" href="js/slick/slick-theme.css">
        <link rel="stylesheet" href="css/style.css">
        <!--endbuild-->
        
        <!--js-->
        <!--build:js js/main.min.js -->
        <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="js/TweenMax.min.js"></script>
        <script type="text/javascript" src="js/slick/slick.min.js"></script>
        <script type="text/javascript" src="js/js_lib.js"></script>
        <script type="text/javascript" src="js/js_run.js"></script>
        <!--endbuild-->

    </head>
    <body>
        <!-- header -->
        <header>
            <div class="wrap-md">
                <div class="head-left">
                    <div class="logo">
                        <a href="index.php"><img src="images/material/logo.png" alt="logo"></a>
                    </div>
                    <ul class="mainmenu">
                        <li><a href="">Home</a></li>
                        <li><a href="">Learn</a></li>
                        <li><a href="">Drivers</a></li>
                        <li><a href="">Media/News</a></li>
                        <li><a href="">Contact</a></li>
                    </ul>
                </div>
                <div class="head-right">
                    <a href="">Log In</a>
                    <a href="" class="btn-box">GET STARTED</a>
                </div>
            </div>
        </header>
        <!-- end of header -->
<?php 
$page = "home";
include('inc_header.php');?>
<!-- middle -->
<section class="bslide">
	<div class="left">
		<div class="slidehome">
			<div class="list">
				<a href=""><figure><img src="images/content/slidehome-01.jpg" alt="slide"></figure></a>
			</div>
			<div class="list">
				<a href=""><figure><img src="images/content/slidehome-02.jpg" alt="slide"></figure></a>
			</div>
		</div>
	</div>
	<div class="right">
		<div class="slidehomenav">
			<div class="list">
				<h3>Case Study</h3>
				<p>First time here?<br>These studies might be helpful</p>

				<div class="lcase">
					<a href="">Defining Your Facebook Advertising Strategy</a>
					<a href="">Effects of List Segmentation on Email Marketing Stats for your brand</a>
					<a href="">Impact of Mobile Use on Email Engagement</a>
					<a href="">Effects of List Segmentation on Email Marketing Stats</a>
				</div>
			</div>
			<div class="list">
				<h3>Case Study</h3>
				<p>First time here? These studies might be helpful</p>

				<div class="lcase">
					<a href="">Defining Your Facebook Advertising Strategy</a>
					<a href="">Effects of List Segmentation on Email Marketing Stats for your brand</a>
					<a href="">Impact of Mobile Use on Email Engagement</a>
					<a href="">Effects of List Segmentation on Email Marketing Stats</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="sintroduce">
	<div class="slidelg">
		<div class="list">
			<a href=""><img src="images/content/homeslidelg.jpg" alt="slide1"></a>
		</div>
		<div class="list">
			<a href=""><img src="images/content/homeslidelg2.jpg" alt="slide2"></a>
		</div>
	</div>
	<div class="boxslidettl">
		<div class="wrap-md">
			<div class="slidettl">
				<div class="list">
					<span>LATEST CASE STUDY</span>
					<h3>Introducing Our First Ever Zero-Emissions Electric Transportation Initiative</h3>
					<a href="" class="btn-box btn-purple">SELENGKAPNYA</a>
				</div>
				<div class="list">
					<span>LATEST CASE STUDY</span>
					<h3>Introducing Our First Ever Zero-Emissions Electric Transportation Initiative</h3>
					<a href="" class="btn-box btn-purple">SELENGKAPNYA</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="newshome">
	<div class="wrap-md">
		<h5>See what we have learned</h5>
		<div class="newslist">
			<div class="ls-news">
				<div class="in-news">
					<a href="">
						<figure><img src="images/content/thumb.jpg" alt="news 1"></figure>
						<div class="desc-news">
							<h6>Introducing Our First Ever Zero-Emissions Electric Transportation</h6>
							<p>Postmates today announced a partnership with GenZe — a manufacturer of electric scooters and bicycles</p>
						</div>
					</a>
				</div>
			</div>
			<div class="ls-news">
				<div class="in-news">
					<a href="">
						<figure><img src="images/content/thumb2.jpg" alt="news 2"></figure>
						<div class="desc-news">
							<h6>Introducing Our First Ever Zero-Emissions Electric Transportation</h6>
							<p>Postmates today announced a partnership with GenZe — a manufacturer of electric scooters and bicycles</p>
						</div>
					</a>
				</div>
			</div>
			<div class="ls-news">
				<div class="in-news">
					<a href="">
						<figure><img src="images/content/thumb3.jpg" alt="news 3"></figure>
						<div class="desc-news">
							<h6>Introducing Our First Ever Zero-Emissions Electric Transportation</h6>
							<p>Postmates today announced a partnership with GenZe — a manufacturer of electric scooters and bicycles</p>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end of middle -->
<?php include('inc_footer.php');?>
<?php 
$page = "about";
include('inc_header.php');?>
<!-- middle -->
<section class="contentwp">
	<section class="submenu">
		<div class="wrap-md">
			<ul>
				<li class="active"><a href="">About us</a></li>
				<li><a href="">Careers</a></li>
				<li><a href="">Partners</a></li>
				<li><a href="">Contact</a></li>
			</ul>
		</div>
	</section>
	<section class="ctnwp">
		<div class="ttlabout">
			<h3>We strive to leverage digital technology to help advertisers meaningfully increase visibility and penetration in a targeted and measurable way.</h3>
		</div>
	</section>
	<section class="story ctnwp">
		<div class="wrap-md">
			<div class="ctnstory">
				<h3>Our Story</h3>
				<p>Founded in 2016 by PT Paragon Pratama Technology, Stickearn is a revolutionary advertising platform that aims to reshape the industry. With its proprietary technology, Stickearn offer advertisers the convenience and flexibility of managing their advertising campaigns in a measurable and efficient way. The resulting campaigns are measurable, offer high ROI, and can be targeted at specific segments. </p>
				<p>Stickearn is a trusted solution used by leading advertisers in major urban centers. For more information, contact us.</p>
			</div>
		</div>
		<div class="imgstory">
			<figure><img src="images/content/story.jpg" alt="story"></figure>
		</div>
	</section>
	<section class="visimisi ctnwp">
		<div class="wrap-sm">
			<h4>Our Vision</h4>
			<h3>To revolutionize the way companies advertise.</h3>
			<h4>Our Mision</h4>
			<h3>“To develop and deliver innovative and effective integrated marketing solutions which help our clients grow their businesses and realize their marketing goals whilst providing the best customer service experience.”</h3>
		</div>
	</section>
	<section class="values ctnwp">
		<div class="wrap-md">
			<h3>Our Values</h3>
			<div class="values-list">
				<div class="lvalues">
					<div class="invalues">
						<div class="num">1</div>
						<div class="dsvalues">
							<h5>Innovative</h5>
							<p>We dare to think differently and try new things.</p>
						</div>
					</div>
				</div>
				<div class="lvalues">
					<div class="invalues">
						<div class="num">2</div>
						<div class="dsvalues">
							<h5>Integrity</h5>
							<p>We show integrity in all our actions.</p>
						</div>
					</div>
				</div>
				<div class="lvalues">
					<div class="invalues">
						<div class="num">3</div>
						<div class="dsvalues">
							<h5>Passionate</h5>
							<p>We care and are excited about what we do.</p>
						</div>
					</div>
				</div>
				<div class="lvalues">
					<div class="invalues">
						<div class="num">4</div>
						<div class="dsvalues">
							<h5>Diversity</h5>
							<p>We believe that diversity is important to making us better, stronger and more creative.</p>
						</div>
					</div>
				</div>
				<div class="lvalues">
					<div class="invalues">
						<div class="num">5</div>
						<div class="dsvalues">
							<h5>Performance</h5>
							<p>Always striving to perform with excellence.</p>
						</div>
					</div>
				</div>
				<div class="lvalues">
					<div class="invalues">
						<div class="num">1</div>
						<div class="dsvalues">
							<h5>Client Value Creation</h5>
							<p>In all actions, create value for our clients. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="findout ctnwp">
		<div class="wrap-md">
			<h3 class="a-center">Find out more about us</h3>
			<div class="afind">
				<div class="lafind">
					<div class="inafind">
						<i class="icwp ic_partnership icafind"></i>
						<h6>Partnerships</h6>
						<p>We’re looking for creative, collaborative individuals.</p>
						<a href="">Learn More</a>
					</div>
				</div>
				<div class="lafind">
					<div class="inafind">
						<i class="icwp ic_career icafind"></i>
						<h6>Careers</h6>
						<p>We’re looking for creative, collaborative individuals.</p>
						<a href="">Learn More</a>
					</div>
				</div>
				<div class="lafind">
					<div class="inafind">
						<i class="icwp ic_gallery icafind"></i>
						<h6>Gallery</h6>
						<p>We’re looking for creative, collaborative individuals.</p>
						<a href="">Learn More</a>
					</div>
				</div>
			</div>

			<h3>Our Location</h3>
			<div class="locationlist">
				<div class="llocation">
					<div class="left">
						<figure><img src="images/content/location-01.jpg" alt="location 1"></figure>
					</div>
					<div class="right">
						<h6>Jakarta</h6>
						<p>Squarespace’s 98,000-square-foot headquarters are located in Manhattan’s West Village at 8 Clarkson Street. The office fills three floors, in addition to a dedicated lobby and roof deck, within the historic Maltz Building. The space was designed with a focus on creating collaborative working spaces for employees to solve problems together creatively.</p>
						<div class="seeloc">
							<i class="icwp ic_marker ic_seeloc"></i>
							<p>Jl Letjen Suprapto 400 Cempaka Putih, Jakarta Pusat 10510, Indonesia</p>
						</div>
					</div>
				</div>
				<div class="llocation">
					<div class="left">
						<figure><img src="images/content/location-02.jpg" alt="location 2"></figure>
					</div>
					<div class="right">
						<h6>Surabaya</h6>
						<p>Squarespace’s 98,000-square-foot headquarters are located in Manhattan’s West Village at 8 Clarkson Street. The office fills three floors, in addition to a dedicated lobby and roof deck, within the historic Maltz Building. The space was designed with a focus on creating collaborative working spaces for employees to solve problems together creatively.</p>
						<div class="seeloc">
							<i class="icwp ic_marker ic_seeloc"></i>
							<p>Jl Letjen Suprapto 400 Cempaka Putih, Jakarta Pusat 10510, Indonesia</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>


<!-- end of middle -->
<?php include('inc_footer.php');?>